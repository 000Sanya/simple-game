
var Imported = Imported || {}
Imported.NullSanya_RespawnEvent = true;

var NullSanya = NullSanya || {}
NullSanya.RespawnEvent = NullSanya.RespawnEvent || {}

if(Imported["OrangeTimeSystem"])
{
    NullSanya.RespawnEvent.WaitingEvents = [];

    NullSanya.RespawnEvent.Scene_Map_onMapLoaded = Scene_Map.prototype.onMapLoaded;
    Scene_Map.prototype.onMapLoaded = function()
    {
        NullSanya.RespawnEvent.Scene_Map_onMapLoaded.call(this);
        for(var i = 0; i < NullSanya.RespawnEvent.WaitingEvents.length; i++)
        {
            var element = NullSanya.RespawnEvent.WaitingEvents[i];
            NullSanya.RespawnEvent.resetEvent(element, i);
        }
    }

    NullSanya.RespawnEvent.Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
    Game_Interpreter.prototype.pluginCommand = function (command, args)
    {
        NullSanya.RespawnEvent.Game_Interpreter_pluginCommand.call(this, command, args);
        if(command === "WaitRespawn")
        {
            NullSanya.RespawnEvent.WaitingEvents.push(
            {
                mapId: $gameMap.mapId(),
                eventId: this._eventId,
                timeToRespawn: Number($dataMap.events[this._eventId].meta["Respawn Time"])  
            });
            $gameSelfSwitches.setValue([$gameMap.mapId(), this._eventId, "A"], true);
        }
    }

    NullSanya.RespawnEvent.onChangeMinute = function()
    {
        console.log("1 minute passess...");
        for(var i = 0; i < NullSanya.RespawnEvent.WaitingEvents.length; i++)
        {
            var element = NullSanya.RespawnEvent.WaitingEvents[i];
            element.timeToRespawn -= 1;
            NullSanya.RespawnEvent.resetEvent(element, i);
        }  
    }
    OrangeTimeSystem.on("changeMinute", NullSanya.RespawnEvent.onChangeMinute);

    NullSanya.RespawnEvent.resetEvent = function(element, i)
    {
        if(element.mapId === $gameMap.mapId() && element.timeToRespawn <= 0)
        {
            console.log("Respawned");
            $gameSelfSwitches.setValue([element.mapId, element.eventId, "A"], false);
            NullSanya.RespawnEvent.WaitingEvents.splice(i, 1);       
        }
    }
}
