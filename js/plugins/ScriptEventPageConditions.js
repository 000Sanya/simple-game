//==
// Script Event Page Conditions v1.0
//==

/*:
 * @plugindesc Even more control over page conditions!
 * @author Vlue
 *
 * @help Script Event Page Conditions! v1.0
 *   Follow me on twitter: https://twitter.com/DaimoniousTails
 *   Or facebook: https://www.facebook.com/DaimoniousTailsGames/
 *   for the most recent updates!
 *  Find all of my work here: http://daimonioustails.weebly.com/
 *  
 *  Just make a comment as the first command of an event page and put:
 *   SC: somescriptcall
 *
 *  And then some script call determines if that page is valid or not! What joy.
 *
 */

(function() {
	
	var sepc_Game_Event_meetsConditions = Game_Event.prototype.meetsConditions
	Game_Event.prototype.meetsConditions = function(page) {
		var pass = sepc_Game_Event_meetsConditions.call(this, page)
		if(pass) { 
			if(page.list[0].code == 108) {
				var com = page.list[0].parameters[0];
				var index = 1;
				while(page.list[index].code == 408) {
					com += page.list[index].parameters[0];
					index++;
				}
				var string = com.match(/SC: (.+)/);
				//console.log(string[1]);
				if(string) { return eval(string[1]); }
			}
		}
		return pass;
	}
	
})();