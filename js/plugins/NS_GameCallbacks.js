var Imported = Imported || {};
Imported.NullSanya_GameCallbacks = true;

var NullSanya = NullSanya || {};
NullSanya.GameCallbacks = NullSanya.GameCallbacks || {};

(function($)
{
    NullSanya.GameCallbacks.onItem_callbacks = [];
    NullSanya.GameCallbacks.onEnemy_callbacks = [];
    NullSanya.GameCallbacks.onGold_callbacks = [];

    NullSanya.GameCallbacks.on = function(eventId, callback)
    {
        nsgc = NullSanya.GameCallbacks;
        if(!(callback && typeof(callback) === "function"))
            return;
        switch (eventId) {
            case "itemChanged":

                break;
            case "enemyDied":
                nsgc.onEnemy_callbacks.push(callback);
                break;
            case "goldChanged":

                break;
            default:

        }
    }

    NullSanya.GameCallbacks.GE_die = Game_BattlerBase.prototype.die;
    Game_Enemy.prototype.die = function()
    {
        $.GE_die.call(this);
        if(this.isActor())
        {

        }
        else if(this.isEnemy())
        {
            for (var i = 0; i < $.onEnemy_callbacks.length; i++) {
                $.onEnemy_callbacks[i].call(this, this._enemyId);
            }
        }
    }
})(NullSanya.GameCallbacks);
